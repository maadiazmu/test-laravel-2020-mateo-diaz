<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // relacionamos los modelos, un usuario con un rol.
    public function relacionUsuarios()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
