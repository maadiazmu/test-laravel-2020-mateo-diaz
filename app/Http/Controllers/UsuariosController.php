<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UsuariosController extends Controller
{
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles(['administrador', 'invidato']);
        $usuarios = User::get();
        return view('usuarios.index')->with('usuarios', $usuarios);
    }
    
    public function crear()
    {
        return view('usuarios.crear');
    }
    
    public function guardar(Request $request)
    {
        $user = new user;
        $user->name = $request->input('nombre');
        $user->email  = $request->input('email');
        $user->password  = $request->input('password');

        $user->save();

        return redirect()->route('usuarios.index');
    }
    public function editar($id)
    {
        $user = User::find($id);
        return view('usuarios.editar')->with('user',$user);
    }

    public function actualizar(Request $request, $id)
    {
        $user = User::find($id);
        $user->nombre = $request->input('nombre');
        $user->email  = $request->input('email');
        $user->save();
        return redirect()->route('usuarios.index');
    }
    public function eliminar($id)
    {
        User::destroy($id);
        return redirect()->route('usuarios.index');
    }
    
}
