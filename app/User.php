<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function relacionRol()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    // metodos tomados de la documentacion de laravel , reconocimiento de roles y reglas de autenticacion

   //este metodo recibe como parametro un rol, si se accede con un rol que no este autorizado por esta funcion se negara el acceso
   //ESTA FUNCION ES LA QUE NOS PERMITIRA PONERLE PERMISOS A LAS VISTAS MEDIANTE LOS CONTROLADORES NECESARIOS
   
    public function authorizeRoles($roles)
        {
            abort_unless($this->hasAnyRole($roles), 401);
            return true;
        }
//como se puede observar la siguiente funcion es utilizada por la funcion authorizeRoles
//en este caso $roles es un arreglo y lo recorremos con un ciclo for each
//muy importante ya que es una funcion booleana

        public function hasAnyRole($roles)
        {
            if (is_array($roles)) {
                foreach ($roles as $role) {
                    if ($this->hasRole($role)) {
                        return true;
                    }
                }
            } else {
                if ($this->hasRole($roles)) {
                    return true; 
                }   
            }
            return false;
        }
        // busca los roles del arreglo $roles
        public function hasRole($role)
        {
            if ($this->roles()->where('name', $role)->first()) {
                return true;
            }
            return false;
        }
    
}
