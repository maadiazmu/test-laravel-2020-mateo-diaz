@extends('layouts.app')

@section('content')
{{ html()->form('POST', route('guardar'))->open() }} 
{{ html()->hidden('id)->value($id)->required() }}
                    <div class="row">
                            <div class="col">
                                <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="nombre usuario" >
                                {{ html()->label('Nombre Usuario') }}
                                {{ html()->label('*') }}
                                {{ html()->text('name')
                                    ->value($user->name)
                                    ->class('form-control')
                                    ->placeholder('Proyecto')
                                    ->required() }}
                                  
                            
                                  
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="email">

                                    {{ html()->label('email') }}
                                    {{ html()->label('*') }}
                                    {{ html()->text('email')
                                    ->value($user->email)
                                    ->class('form-control')
                                    ->placeholder('escriba el email')
                                    ->required() }}
                                    
                            
                                   
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
        <button type="submit" class="btn btn-success btn-block">Guardar</button>
                 {{ html()->form()->close() }}

@endsection