@extends('layouts.app')

@section('content')
<div class="container">
<a class="btn btn-success pull-right" href="{{ url('/usuarios/crear') }}" role="button">Crear Usuario</a>
<div class="card">
           
                <div class="card-body">


                <table class="table table-bordered">
                          <thead>
                            <tr>
                              
                              <th scope="col">id usuario</th>
                              <th scope="col">Nombre usuario</th>
                              <th scope="col">email</th>
                              <th scope="col">Opciones</th>
                            
                            
                            </tr>
                          </thead>
                          <tbody>
                          @if( is_array($usuarios) || is_object($usuarios) )
                              @foreach($usuarios as $usuario)
                            <tr>
                              
                          
                              <td>{{$usuario->id}}</td>
                              <td>{{$usuario->name}}</td>
                              <td>{{$usuario->email}}</td>
                              
                   
                              <td>
                              
                                  <a  href="" class="btn btn-xs btn-info">
                                  <i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                  </a> 
                                  <a  href="" class="btn btn-xs btn-info">
                                  <i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
                                  </a> 
              
                                        
                              </td>                           
                              
                            
                            </tr>
                            @endforeach
                            @endif
                          </tbody>
                      </table>
                    
                    
                </div>
                
  
            </div>

</div>
@endsection