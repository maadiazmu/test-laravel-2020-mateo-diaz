<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);//llamamos el seeder de la creacion de los roles
        $this->call(UsuarioTableSeeder::class);//llamamos el seeder de la creacion del usuario de ejemplo
    }
}
