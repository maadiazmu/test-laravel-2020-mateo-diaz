<?php

use Illuminate\Database\Seeder;
use App\User;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin@example.com';
        $user->email = 'admin@example.com';
        $user->password = '123';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo1@example.com';
        $user->email = 'ejemplo1@example.com';
        $user->password = 'ejemplo1';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo2@example.com';
        $user->email = 'ejemplo2@example.com';
        $user->password = 'ejemplo2';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo3@example.com';
        $user->email = 'ejemplo3@example.com';
        $user->password = 'ejemplo3';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo4@example.com';
        $user->email = 'ejemplo4@example.com';
        $user->password = 'ejemplo4';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo5@example.com';
        $user->email = 'ejemplo5@example.com';
        $user->password = 'ejemplo5';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo6@example.com';
        $user->email = 'ejemplo6@example.com';
        $user->password = 'ejemplo6';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo7@example.com';
        $user->email = 'ejemplo7@example.com';
        $user->password = 'ejemplo7';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo8@example.com';
        $user->email = 'ejemplo8@example.com';
        $user->password = 'ejemplo8';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo9@example.com';
        $user->email = 'ejemplo9@example.com';
        $user->password = 'ejemplo9';
        $user->save();
        $user = new User();
        $user->name = 'ejemplo10@example.com';
        $user->email = 'ejemplo10@example.com';
        $user->password = 'ejemplo10';
        $user->save();

    }
}
